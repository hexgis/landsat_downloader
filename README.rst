============
landsat_downloader
============

.. image:: https://img.shields.io/pypi/v/landsat_downloader.svg
        :target: https://pypi.python.org/pypi/landsat_downloader

.. image:: https://img.shields.io/travis/dagnaldo/landsat_downloader.svg
        :target: https://travis-ci.org/dagnaldo/landsat_downloader

.. image:: https://readthedocs.org/projects/landsat-downloader/badge/?version=latest
        :target: https://landsat-downloader.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


A python package used for download landsat images from EarthExplorer

* Free software: GNU General Public License v3
* Documentation: https://landsat-downloader.readthedocs.io.

Features:
--------

* Download scenes:
	.. code-block:: python
    
		from datetime import datetime, timedelta

		from landsat_downloader.downloader import LandsatDownloader

		LandsatDownloader.download_scene(
			bands=[6, 5, 4, 'BQA'],
			scene_id = "LC82240692018053LGN00,
			product_id = "LC08_L1GT_224069_20180222_20180308_01_T2",
			metadata=True,
			)

* TODO

- Create CLI;

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
